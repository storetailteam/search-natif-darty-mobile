var settings = require("./settings.json");
var timeName = new Date().getTime().toString(36);
settings.container_color = settings.container_color === "" ? "none" : settings.container_color;
settings.button_color = settings.button_color === "" ? "none" : settings.button_color;
settings.container_img = settings.container_img === "" ? "none" : `url(./../../img/${settings.container_img})`;
settings.button_img = settings.button_img === "" ? "none" : `url(./../../img/${settings.button_img})`;
settings.color_theme = settings.color_theme === "" ? "none" : settings.color_theme;
settings.banner_icon = settings.banner_icon === "" ? "none" : `url(./../../img/${settings.banner_icon})`;
module.exports = function (source) {
    this.cacheable();
    var test = source
    .replace(/__PLACEHOLDER__/g, `${settings.format}_${settings.name}`)
    .replace(/__FORMAT__/g, settings.format)
    .replace(/__CONTAINER_COLOR__/g, settings.container_color)
    .replace(/__BUTTON_COLOR__/g, settings.button_color)
    .replace(/__CONTAINER_IMG__/g, settings.container_img)
    .replace(/__BUTTON_IMG__/g, settings.button_img)
    .replace(/__CONTAINERIMG__/g,settings.img_container)
    .replace(/__COLORTHEME__/g,settings.color_theme)
    .replace(/__BANNERICON__/g,settings.banner_icon);
    return test;
};
