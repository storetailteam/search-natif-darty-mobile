"use strict";
var sto = window.__sto,
  helper_methods = sto.utils.retailerMethod,
  settings = require("../../settings"),
  $ = window.jQuery,
  placeholder = `${settings.format}_${settings.name}`,
  formatID = `${placeholder}_${Math.random().toString(36).substring(7)}`,
  style = require("./main.css"),
  htmlFormat = require("./main.html"),
  formatDisplay = $(htmlFormat),
  containercss = require("./main.css"),
  redirect = settings.redirect,
  target = settings.target,
  format = settings.format;
var executed = false;


module.exports = {
  init: _init_(),
}

function _init_() {
  sto.load(format, function(tracker) {
    if ($(".site-body>.well>#ajax-list").length > 0) {
      
      var container = $(htmlFormat),
        helper_methods = sto.utils.retailerMethod;
      helper_methods.crawl(settings.crawl).promise.then(function(d) {
        style.use();
        var form = helper_methods.createFormat(container, tracker, settings.product, d);
        $(".site-body>.well>#ajax-list").prepend(container);
        if ($('.sto-' + placeholder + '-container .darty_product_img img').attr('src').indexOf("blank") >= 0) {
          var goodImgSrc = $('.sto-' + placeholder + '-container .darty_product_img img').attr('data-original');
          $('.sto-' + placeholder + '-container .darty_product_img img').attr('src', goodImgSrc);
        }
      });
      setTimeout(function() {
        $(document).find(".sto_format .btn-comparator-add span").click(function() {
          tracker.sendHit({
            "ta": "clk",
            "tl": "comparer"
          });
        });
        $(document).find(".sto_format .flag-mkp-multi a").click(function() {
          tracker.sendHit({
            "ta": "clk",
            "tl": "offres"
          });
        });
      }, 200);
    }
  });
}